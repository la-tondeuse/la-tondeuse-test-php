# Mower Simulation

## Description

Ce projet simule le mouvement de tondeuses sur une grille en suivant des instructions fournies dans un fichier d'entrée. Chaque tondeuse a une position initiale et une série d'instructions pour se déplacer sur la grille.

## Fichiers

Mower.php: Contient le code source pour simuler les mouvements des tondeuses.
input.txt: Fichier d'entrée contenant les dimensions de la grille et les positions initiales et instructions pour chaque tondeuse.

## Instructions
Format du fichier d'entrée
Le fichier d'entrée doit être structuré comme suit :

1. La première ligne contient les dimensions de la grille (coordonnées maximales x et y).
1. Chaque paire de lignes suivante représente une tondeuse:

 
- La première ligne de la paire contient la position initiale de la tondeuse (coordonnées x, y et orientation).
- La deuxième ligne de la paire contient les instructions pour la tondeuse (D pour tourner à droite, G pour tourner à gauche, A pour avancer).

Exemple de input.txt :

`5 5
GAGAGAGAA
3 3 E
AADAADADDA
`

## Exécution du programme

1. Assurez-vous que le fichier input.txt est présent dans le même répertoire que Mower.php.
1. Exécutez le script PHP depuis la ligne de commande :
#### php Mower.php

## Exemple de sortie

Pour l'exemple de fichier d'entrée donné ci-dessus, la sortie du programme sera :

`1 3 N
5 1 E`

## Explication du Code

### Classe Mower
Cette classe représente une tondeuse avec les propriétés suivantes :

- x, y : Coordonnées actuelles de la tondeuse.
- orientation : Orientation actuelle de la tondeuse (N, E, W, S).
- maxX, maxY : Coordonnées maximales de la grille.

### Méthodes

- __construct($x, $y, $orientation, $maxX, $maxY) : Initialise la tondeuse avec ses coordonnées, orientation et les limites de la grille.
- executeInstructions($instructions) : Exécute une série d'instructions pour déplacer la tondeuse.
- turnRight() : Pivote la tondeuse à droite.
- turnLeft() : Pivote la tondeuse à gauche.
- moveForward() : Avance la tondeuse d'une case dans la direction actuelle.
- getPosition() : Retourne la position actuelle de la tondeuse sous forme de chaîne de caractères.

### Fonctions Utilitaires

- readInputFile($filename) : Lit le fichier d'entrée et extrait les dimensions de la grille et les données des tondeuses.
- main($inputFilename) : Fonction principale pour exécuter le programme. Lit les données du fichier d'entrée, initialise les tondeuses, exécute leurs instructions et affiche leurs positions finales.













