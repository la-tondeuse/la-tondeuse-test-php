<?php

// Classe représentant une tondeuse
class Mower {
    private $x;  // Coordonnée x actuelle de la tondeuse
    private $y;  // Coordonnée y actuelle de la tondeuse
    private $orientation;  // Orientation actuelle de la tondeuse (N, E, W, S)
    private $maxX;  // Coordonnée x maximale de la grille
    private $maxY;  // Coordonnée y maximale de la grille

    // Constructeur initialisant la tondeuse avec ses coordonnées, orientation et les limites de la grille
    public function __construct($x, $y, $orientation, $maxX, $maxY) {
        $this->x = $x;
        $this->y = $y;
        $this->orientation = $orientation;
        $this->maxX = $maxX;
        $this->maxY = $maxY;
    }

    // Méthode pour exécuter une série d'instructions (D, G, A)
    public function executeInstructions($instructions) {
        // Parcourir chaque instruction
        foreach (str_split($instructions) as $instruction) {
            // Exécuter l'instruction correspondante
            switch ($instruction) {
                case 'D':
                    $this->turnRight();  // Pivoter à droite
                    break;
                case 'G':
                    $this->turnLeft();  // Pivoter à gauche
                    break;
                case 'A':
                    $this->moveForward();  // Avancer
                    break;
            }
        }
    }

    // Méthode pour pivoter à droite
    private function turnRight() {
        // Définir les orientations après un pivotement à droite
        $orientations = ['N' => 'E', 'E' => 'S', 'S' => 'W', 'W' => 'N'];
        $this->orientation = $orientations[$this->orientation];
    }

    // Méthode pour pivoter à gauche
    private function turnLeft() {
        // Définir les orientations après un pivotement à gauche
        $orientations = ['N' => 'W', 'W' => 'S', 'S' => 'E', 'E' => 'N'];
        $this->orientation = $orientations[$this->orientation];
    }

    // Méthode pour avancer d'une case dans la direction actuelle
    private function moveForward() {
        // Avancer dans la direction de l'orientation
        switch ($this->orientation) {
            case 'N':  // Vers le nord
                if ($this->y < $this->maxY) $this->y++;  // Avancer si pas en dehors de la grille
                break;
            case 'E':  // Vers l'est
                if ($this->x < $this->maxX) $this->x++;  // Avancer si pas en dehors de la grille
                break;
            case 'S':  // Vers le sud
                if ($this->y > 0) $this->y--;  // Avancer si pas en dehors de la grille
                break;
            case 'W':  // Vers l'ouest
                if ($this->x > 0) $this->x--;  // Avancer si pas en dehors de la grille
                break;
        }
    }

    // Méthode pour obtenir la position actuelle de la tondeuse sous forme de chaîne de caractères
    public function getPosition() {
        return $this->x . ' ' . $this->y . ' ' . $this->orientation;
    }
}

// Fonction pour lire le fichier d'entrée et extraire les données
function readInputFile($filename) {
    $file = fopen($filename, "r");  // Ouvrir le fichier en lecture
    if (!$file) {
        die("Unable to open file!");  // Arrêter le programme si le fichier ne peut être ouvert
    }

    // Lire la première ligne pour obtenir les dimensions de la grille
    $maxCoords = explode(' ', trim(fgets($file)));
    $maxX = (int)$maxCoords[0];  // Coordonnée x maximale
    $maxY = (int)$maxCoords[1];  // Coordonnée y maximale

    $mowers = [];  // Tableau pour stocker les données des tondeuses

    // Lire les lignes suivantes pour les positions et instructions des tondeuses
    while (($line = fgets($file)) !== false) {
        // Lire la position initiale de la tondeuse
        $initialPosition = explode(' ', trim($line));
        
        // Vérifier que $initialPosition contient bien trois éléments
        if (count($initialPosition) < 3) {
            continue;  // Ignorer les lignes qui n'ont pas assez de données
        }

        // Extraire les coordonnées et l'orientation initiales
        $x = (int)$initialPosition[0];
        $y = (int)$initialPosition[1];
        $orientation = $initialPosition[2];

        // Lire les instructions pour la tondeuse
        $instructions = trim(fgets($file));

        // Ajouter les données de la tondeuse au tableau
        $mowers[] = ['x' => $x, 'y' => $y, 'orientation' => $orientation, 'instructions' => $instructions];
    }

    fclose($file);  // Fermer le fichier

    // Retourner les dimensions de la grille et les données des tondeuses
    return [$maxX, $maxY, $mowers];
}

// Fonction principale pour exécuter le programme
function main($inputFilename) {
    // Lire les données du fichier d'entrée
    list($maxX, $maxY, $mowers) = readInputFile($inputFilename);
    $results = [];  // Tableau pour stocker les résultats

    // Pour chaque tondeuse
    foreach ($mowers as $mowerData) {
        // Initialiser la tondeuse avec ses données
        $mower = new Mower($mowerData['x'], $mowerData['y'], $mowerData['orientation'], $maxX, $maxY);

        // Exécuter les instructions de la tondeuse
        $mower->executeInstructions($mowerData['instructions']);

        // Ajouter la position finale de la tondeuse aux résultats
        $results[] = $mower->getPosition();
    }

    // Afficher les positions finales des tondeuses
    foreach ($results as $result) {
        echo $result . PHP_EOL;
    }
}

// Lancer le programme avec le fichier d'entrée "input.txt"
main('input.txt');

